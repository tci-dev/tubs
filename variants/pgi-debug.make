TUBS_FFLAGS     ?= -g -fPIC -mp

TUBS_LDFLAGS  ?= -fPIC -mp

include $(TCI_MAKE_HERE)/variants/pgi_common.make

#EOF vim:syntax=make:foldmethod=marker:ts=4:noexpandtab:
