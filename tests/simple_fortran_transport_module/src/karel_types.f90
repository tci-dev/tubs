module karel_types
  implicit none
  integer, parameter :: karel_dp=8
  !! Karel-model-style 'double precision' floating point numbers
end module karel_types
