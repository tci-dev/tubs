# vim:filetype=make

# Main use case is lac8 at EPFL/TCV
# lac8 has an intel 2016 compiler
# See /opt/intel/compilers_and_libraries_2016.0.109/
# Set your vars with
# source /opt/intel/bin/compilervars.sh intel64 -platform linux
# No MPI available
ifeq ($(TUBS_CONFIG_PHASE),1)
  ifeq ($(TUBSCFG_MPI_FROM_DEFAULT), 1)
    TUBSCFG_MPI := 0
  endif
  ifeq ($(TUBSCFG_MKL_FROM_DEFAULT), 1)
    TUBSCFG_MKL := 1
  endif
  # Check if we are compiling with MPI
  ifeq ($(TUBSCFG_MPI),1)
    $(error Detected MPI compilation LAC has no MPI, TUBSCFG_MPI=$(TUBSCFG_MPI))
  else ifeq ($(TUBSCFG_MPI),0)
    $(info Serial compilation, TUBSCFG_MKL=$(TUBSCFG_MKL))
  endif
else ifeq ($(TUBS_CONFIG_PHASE),2)
  # Check if we are compiling with MKL
  ifeq ($(TUBSCFG_MKL),1)
    $(info Detected MKL compilation, TUBSCFG_MKL=$(TUBSCFG_MKL))
    # If we compile with MKL
    ifeq ($(TOOLCHAIN),intel)
      $(info Detected TOOLCHAIN=$(TOOLCHAIN))
      TUBS_FFLAGS += -i8 -I${MKLROOT}/include/intel64/ilp64 -I${MKLROOT}/include
      MKL_LDFLAGS += -i8 # Needed for MPI linking with INTEL MPI to go correctly
      MKL_LDFLAGS += ${MKLROOT}/lib/intel64/libmkl_blas95_ilp64.a
      MKL_LDFLAGS += ${MKLROOT}/lib/intel64/libmkl_lapack95_ilp64.a
      MKL_LDFLAGS += -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_ilp64.a ${MKLROOT}/lib/intel64/libmkl_sequential.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm -ldl
    else
      $(error Unknown TOOLCHAIN=$(TOOLCHAIN))
    endif

  else ifeq ($(TUBSCFG_MKL),0)
    $(info Not compiling with MKL, TUBSCFG_MKL=$(TUBSCFG_MKL))
  else
    $(info Unknown TUBSCFG_MKL $(TUBSCFG_MKL))
  endif

  LOCALCFG_ACTIVE := epfl.ch
else
  $(error Unknown TUBS_CONFIG_PHASE $(TUBS_CONFIG_PHASE))
endif

